import { configureStore } from '@reduxjs/toolkit';
// import counterReducer from '../features/counter/counterSlice';
import pokemonReducer from '../features/PokemonSlice';
export const store = configureStore({
  reducer: {
    pokemon: pokemonReducer,
  },
});
