import React from 'react';
import './Paginate.css';

function Paginate(props) {
    return (
        <div className="card__post--pagination">
            <button className="post__pagination text-decoration-none f-inter-700 btn btn-success">1</button>
            <button className="post__pagination text-decoration-none f-inter-700 btn btn-success">2</button>
            <button className="post__pagination text-decoration-none f-inter-700 btn btn-success">3</button>
            <button className="post__pagination text-decoration-none f-inter-700 btn btn-success">4</button>
        </div>
    )
}

Paginate.propTypes = {

}

export default Paginate

