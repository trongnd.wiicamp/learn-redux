import React, { useEffect, useState } from 'react';
import './Main.css';
import { useDispatch } from 'react-redux';

const Main = () => {
    const [pokemonList, setPokemonList] = useState([]);
    const namePokemon = [
        {
            id: 1,
            name: 'rattata',
            src: 'https://lh3.googleusercontent.com/proxy/VapjCjniqo8B5KwfH--D9y9PIz8CvDQVCBKA58ZKKVopkSAGkYiQOpYKNKBxrEknsUl8CWvamOaxgFtwAm3I6q2rHZhwmcDKgjCeQ5XxA7al3gmXw78WZmn1MgRlTi9XCEH1'
        },
        {
            id: 2,
            name: 'raticate',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/20-Raticate.webp'
        },
        {
            id: 3,
            name: 'spearow',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/21-Spearow.webp'
        },
        {
            id: 4,
            name: 'fearow',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/22-Fearow.webp'
        },
        {
            id: 5,
            name: 'ekans',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/23-Ekans.webp'
        },
        {
            id: 6,
            name: 'arbok',
            src: 'https://images.gameinfo.io/pokemon/256/024-00.png'
        },
        {
            id: 7,
            name: 'pikachu',
            src: 'https://upload.wikimedia.org/wikipedia/vi/thumb/f/f7/Sugimoris025.png/220px-Sugimoris025.png'
        },
        {
            id: 8,
            name: 'raichu',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/26-Raichu.webp'
        },
        {
            id: 9,
            name: 'sandshrew',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/27-Sandshrew.webp'
        },
        {
            id: 10,
            name: 'sandslash',
            src: 'https://serebii.net/swordshield/pokemon/028.png'
        },
        {
            id: 11,
            name: 'nidoran-f',
            src: 'http://www.pokemaniablog.com/assets/img/nidorans.png'
        },
        {
            id: 12,
            name: 'nidorina',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/30-Nidorina.webp'
        },
        {
            id: 13,
            name: 'nidoqueen',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/30-Nidorina.webp'
        },
        {
            id: 14,
            name: 'nidoran-m',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/31-Nidoqueen.webp'
        },
        {
            id: 15,
            name: 'nidorino',
            src: 'https://lh3.googleusercontent.com/proxy/bi1N2C6NqQVC-wgAWMXgZkUATMH1IC_Hrc8E1VIBH3u7BrCJFIw6FGETI2whyQvcSq0t-Tkq2DvutrPwdNXZ_1d7fgF-w38PWRfZJwB9cYRsc4mbmCVuyZejcPqS1FNl'
        },
        {
            id: 16,
            name: 'nidoking',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/33-Nidorino.webp'
        },
        {
            id: 17,
            name: 'clefairy',
            src: 'http://static.pokemonpets.com/images/monsters-images-800-800/2035-Shiny-Clefairy.webp'
        },
        {
            id: 18,
            name: 'clefable',
            src: 'https://static.pokemonpets.com/images/monsters-images-300-300/36-Clefable.webp'
        },

    ]
    useEffect(() => {
        async function fetchPokemonList() {
            const requestUrl = 'https://pokeapi.co/api/v2/pokemon?offset=18&limit=6/berry/id';
            const response = await fetch(requestUrl);
            const responseJSON = await response.json();
            setPokemonList(responseJSON.results);
        }
        fetchPokemonList();
    }, [])
    console.log(pokemonList);
    return (
        <div className="container main">
            <div className="title">Pokemon List</div>
            <div className="row justify-content-center">
                {pokemonList.map(pokemon => {
                    let index = namePokemon.findIndex(item => item.name === pokemon.name);
                    // console.log(index);
                    return (
                        <div className="col-lg-4 mb-4" key={Math.random(2 * 100)}>
                            <div className="wrapper">
                                <div className="img__box">
                                    <img className="pokemon__img" src={namePokemon[index].src} alt="Name pokemon" />
                                </div>
                                <div className="linear-layout">
                                    <p className="wrapper__title">{pokemon.name}</p>
                                    <button className="btn btn-success">View Detail</button>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>

        </div>

    );
};

Main.propTypes = {

};

export default Main;