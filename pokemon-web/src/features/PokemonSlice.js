import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    pokemonList: [{
        pages: 1
    }]
}

const PokemonSlice = createSlice({
    name: 'pokemon',
    initialState,
    reducers: {

    }
});

export const { addFistData } = PokemonSlice.actions;
export const selectPokemon = state => state.pokemon.pokemonList;
export default PokemonSlice.reducer