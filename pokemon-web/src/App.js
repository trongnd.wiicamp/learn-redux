import React from 'react';
import {
  BrowserRouter as Router,
  // Switch,
  Route,
  Link,
  Switch
} from "react-router-dom";
import './App.css';
import AddPokemon from './components/pages/Add/AddPokemon';
import Main from './components/pages/Main/Main';
import Paginate from './components/pages/Pagination/Paginate';
// import Main from './components/pages/Main/Main';

function App() {
  return (
    <Router>
      <div className="container">
        <div className="d-flex justify-content-between align-items-center pt-2 pb-2">
          <Link to="/"><img className="logo" width="10%" height="10%" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png" alt="" /></Link>
          <Link to="/addpokemon">Buy Pokemon</Link>
        </div>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/addpokemon">
            <AddPokemon />
          </Route>
          <Route path="/">
            <Main />
            <Paginate />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
