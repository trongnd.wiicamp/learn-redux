// rsxlice -> tab
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    todoList: [{
        name: 'Read 1 book',
        done: true,
        id: 323
    },
    {
        name: 'Learning English',
        done: false,
        id: 123123
    },
    {
        name: 'Play game',
        done: false,
        id: 345345
    },]
}

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        saveTodo: (state, action) => {
            // console.log(active);
            state.todoList.push(action.payload);
        },

        setCheck: (state, action) => {
            state.todoList.map(item => {
                if (action.payload === item.id) {
                    if (item.done === true) {
                        item.done = false;
                    } else {
                        item.done = true;
                    }
                }
                return 0;
            })
        },

        removeTodo: (state, action) => {
            const index = state.todoList.findIndex(item => item.id === action.payload);
            state.todoList.splice(index, 1);
        }
    }
});

export const { saveTodo, setCheck, removeTodo } = todoSlice.actions;

export const selectTodoList = state => state.todos.todoList;

export default todoSlice.reducer;