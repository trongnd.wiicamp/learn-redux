import React from 'react'
import Checkbox from '@material-ui/core/Checkbox';
import './todoitem.css';

import { useDispatch } from 'react-redux';
import { setCheck, removeTodo } from '../features/todoSlice';

export const TodoItem = ({ name, id, done, test }) => {
    const dispatch = useDispatch();

    const handleCheck = () => {
        dispatch(setCheck(id));
    }

    const handleRmove = () => {
        dispatch(removeTodo(id));
    }

    return (
        <div className="todoItem" key={test}>
            <div className="d-flex align-items-center " >
                <Checkbox
                    checked={done}
                    color="primary"
                    onChange={handleCheck}
                    inputProps={{ 'aria-label': 'secondary-checkbox' }}
                />

                <p className={done ? 'todoItem--done m-0' : 'm-0'}>{name}</p>
            </div>
            <button className="remove" data-set={id} onClick={handleRmove}>remove</button>
        </div>
    )
}

export default TodoItem;
