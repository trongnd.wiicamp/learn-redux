import React, { useState } from 'react';
import './input.css';

import { useDispatch } from 'react-redux';
import { saveTodo } from '../features/todoSlice';

const Input = () => {
	const [value, setValue] = useState('');
	const dispatch = useDispatch();

	const addTodo = () => {
		dispatch(saveTodo({
			name: value,
			done: false,
			id: Date.now()
		}))
	}

	return (
		<div className="input">
			<input
				type="text"
				value={value}
				onChange={(e) => setValue(e.target.value)}
			/>

			<button type="submit" onClick={addTodo}>Add!</button>
		</div>
	)
}
export default Input;
