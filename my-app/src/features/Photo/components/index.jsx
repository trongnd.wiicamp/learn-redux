import React from 'react'
import PropTypes from 'prop-types'
import './photoform.scss';
import { PHOTO_CATEGORY_OPTIONS } from '../../../constans/global';
import { FastField, Form, Formik } from 'formik';
import { Button, FormGroup, Spinner } from 'reactstrap';
import InputField from '../../../custom-fields/InputField';
import SelectField from '../../../custom-fields/SelectField/SelectField';
import RandomPhotoField from '../../../components/RandomPhotoField';
import * as Yup from 'yup';

Photoform.propTypes = {
  onSubmit: PropTypes.func
}

Photoform.defaultProps = {
  onSubmit: null
}

function Photoform({ onSubmit }) {
  const initialValues = {
    title: '',
    categoryId: null,
    photo: ''
  }

  const valudationSchema = Yup.object().shape({
    title: Yup.string().required('This field is required.'),

    categoryId: Yup.number().nullable().required('This field is required.'),
    // Chỉ có category là có value === thì mới bắt buộc photo
    // Và đây chọn id === 1

    photo: Yup.string().when('categoryId', {
      is: 1,
      then: Yup.string().required('This field is required.'),
      otherwise: Yup.string().notRequired()
    })
  })

  return (
    <div className="container photo__form--style">

      <Formik
        initialValues={initialValues}
        validationSchema={valudationSchema}
        onSubmit={onSubmit}
      >
        {formikProps => {
          const { isSubmitting } = formikProps;
          return (
            <Form>
              {/* re-render khi tasc ddoong towis field */}
              <FastField
                name="title"
                component={InputField}

                label="Title"
                placeholder="Eg: Wow nature ..."
              >
              </FastField>

              <FastField
                name="categoryId"
                component={SelectField}

                label="Category"
                placeholder="What's your photo category"
                options={PHOTO_CATEGORY_OPTIONS}
              >
              </FastField>

              <FastField
                name="photo"
                component={RandomPhotoField}
                label="Photo"
              />

              <FormGroup>
                <Button type="submit" >
                  {isSubmitting && <Spinner size="sm" />}
                  Add to album
                </Button>
              </FormGroup>

            </Form>
          )
        }}
      </Formik>


    </div >
  )
}

export default Photoform

