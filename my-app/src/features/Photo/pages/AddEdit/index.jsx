import React from 'react'
import Banner from '../../../../components/Banner'
import Photoform from '../../components';
import { addPhoto } from '../Main/photoSlice';
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom';
function AddEdit(props) {
    const dispatch = useDispatch();
    const history = useHistory();

    const handleSubmit = (values) => {
        return new Promise(resolve => {
            setTimeout(() => {
                const action = addPhoto(values);
                console.log(action);
                dispatch(action);

                history.push('/photos');
                resolve(true)
            }, 2000)
        })
    }

    return (
        <div className="photo-edit">
            <Banner title="Pick your amazing photo 🎉" />

            <div className="photo-edit__form">
                <Photoform onSubmit={handleSubmit} />
            </div>
        </div>
    )
}

AddEdit.propTypes = {

}

export default AddEdit

