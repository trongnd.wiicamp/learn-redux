import React from 'react'
import Banner from '../../../../components/Banner'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux';

function MainPage(props) {
    const photos = useSelector(state => state.photos);
    console.log('lush', photos);
    return (
        <div className="photo-main">
            <Banner title="Your awesome photos" backgroundUrl={"https://png.pngtree.com/thumb_back/fh260/background/20201010/pngtree-watercolor-gradient-yellow-orange-background-image_409268.jpg"} />
            <div className="container text-center">
                <Link to="/photos/add">Add new photo</Link>
            </div>
        </div>
    )
}

MainPage.propTypes = {

}

export default MainPage

