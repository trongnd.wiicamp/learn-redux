import PropTypes from 'prop-types';
import React from 'react';
import { FormFeedback, FormGroup, Label } from 'reactstrap';
import Select from 'react-select';
import { ErrorMessage } from 'formik';
// import Select from 'react-select';

SelectField.propTypes = {
    field: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,

    disabled: PropTypes.bool,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    options: PropTypes.array,
};

SelectField.defaultProps = {
    options: [],
    label: '',
    placeholder: '',
    disabled: false,
}

function SelectField(props) {
    const {
        field,
        options, form, label, placeholder, disabled,
    } = props;
    const { name, value } = field;
    const selectedOption = options.find(option => option.value === value);
    const { errors, touched } = form;

    const showError = errors[name] && touched[name];


    const handleSelectedOptionChange = (selectedOption) => {
        const selectedValue = selectedOption ? selectedOption.value : selectedOption;
        const changeEvent = {
            target: {
                name: name,
                value: selectedValue,
            }
        };
        field.onChange(changeEvent);
    }

    return (
        <FormGroup>
            {label && <Label for={name}>{label}</Label>}

            <Select
                id={name}
                {...field}
                disabled={disabled}
                value={selectedOption}
                onChange={handleSelectedOptionChange}
                placeholder={placeholder}
                options={options}
            // Phải có class is-invalid thì formFeedback mới haotj động
            // Bên formik hỗ trợ sẵn invalid, còn select thì ko
            // className={showError ? 'is-invalid' : ''}
            />
            <div className={showError ? 'is-invalid' : ''}></div>
            <ErrorMessage name={name} component={FormFeedback} />
        </FormGroup>
    );
}

export default SelectField;