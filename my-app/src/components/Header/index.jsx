import React from 'react'
import { Link } from 'react-router-dom'

function Header(props) {
    return (
        <div className="container mt-5 mb-5">
            <div className="d-flex justify-content-between">
                <h1>Easy Frontend</h1>
                <Link to="/photos"><h2>Redux home</h2></Link>
            </div>
        </div>
    )
}

export default Header

