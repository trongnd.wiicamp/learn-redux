import React from 'react'
import PropTypes from 'prop-types'
import './banner.scss';

Banner.propTypes = {
    title: PropTypes.string,
    backgroundUrl: PropTypes.string,
}

Banner.defaultProps = {
    title: '',
    backgroundUrl: '',
}

function Banner({ title, backgroundUrl }) {
    console.log(backgroundUrl);
    const bannerStyle = backgroundUrl ? { background: `url(${backgroundUrl})` } : {}
    console.log(bannerStyle);
    return (
        <section className="banner" style={bannerStyle}>
            <h1 className="banner__title">{title}</h1>
        </section>
    )
}

export default Banner

